<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  protected $fillable = [
    'category_id',
    'title',
    'url'
  ];

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

}
