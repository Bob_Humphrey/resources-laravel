<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

  public function __construct() {
    $this->middleware('auth', ['only' => ['store', 'update', 'destroy']]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       $categories = Category::orderBy('name')->get();
       return view('category.index', [
         'categories' => $categories,
         ]);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate(request(), [
        'name' => 'required|unique:categories,name',
      ]);
      Category::create(request(['name']));
      $request->session()->flash('status',  request()->name . ' has been added.');
      return redirect(action('CategoryController@index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $category = Category::find($id);
      return view('category.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate(request(), [
        'name' => 'required|unique:categories,name',
      ]);

      $category = Category::find($id);
      $category->name = request()->name;
      $category->save();
      $request->session()->flash('status', request()->name . ' has been updated.');
      return redirect(action('CategoryController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = Category::find($id);
      $name = $category->name;
      $category->delete();
      session()->flash('status', $name . ' has been deleted.');
      return redirect(action('CategoryController@index'));
    }

      /**
       * Display the specified resource to be deleted.
       * An extra step to protect against accidental deletes.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function delete($id)
      {
          $category = Category::find($id);
          return view('category.delete', ['category' => $category]);
      }
}
