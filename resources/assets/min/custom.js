// Single category

u('h3.category').on('click', function(){
  if(u(this).siblings('div').hasClass('hidden')) {
    u(this).siblings('div').removeClass('hidden');
  }
  else {
    u(this).siblings('div').addClass('hidden');
  }
});

// Show all categories

u('.show-all').on('click', function(){
  u('.items').each(function(node, i){
    u(node).removeClass('hidden');
  });
});

// Hide all categories

u('.hide-all').on('click', function(){
  u('.items').each(function(node, i){
    u(node).addClass('hidden');
  });
});
