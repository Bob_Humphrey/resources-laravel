@extends('layouts.app')

@section('content')

@php
  $urlActionDestroy = action('ItemController@destroy', ['id' => $item->id]);
  $title = $item->title;
  $url = $item->url;
  $disabled = 'disabled';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans mb-2">Delete Resource</h2>
    <div class="font-sans text-red font-bold mb-4">
      Warning! This action cannot be undone.
    </div>

    @include('item.form', [
      'parent' => 'delete',
      'formMethod' => 'POST',
      'formAction' => $urlActionDestroy
      ])

  </div>
</div>

@endsection
