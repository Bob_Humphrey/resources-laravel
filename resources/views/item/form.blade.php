<form class="form" method="{{ $formMethod }}"
action="{{ $formAction }}">

  {{ csrf_field() }}

  @if ($parent === 'edit')
    <input name="_method" type="hidden" value="PATCH">
  @elseif ($parent === 'delete')
    <input name="_method" type="hidden" value="DELETE">
  @endif

  <div class="md:flex items-center mb-3">
  <div class="md:w-1/6">
    <label class="form-label md:text-right mb-1" for="category_id">
      Category
    </label>
  </div>
  @if ($disabled)
    <div class="md:w-5/6">
      <input class="form-text" id="category_id" name="category_id"
        type="text" value="{{ $itemCategory->name }}" {{ $disabled }}>
    </div>
  @else
    <div class="md:w-5/6">
      <select class="form-text" id="category_id" name="category_id">
        @foreach ($categories as $category)
          <option value="{{ $category->id }}" {{ $category->selected }}>
            {{ $category->name }}
          </option>
        @endforeach
      </select>
    </div>
  @endif
  </div>

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="form-label md:text-right mb-1" for="title">
        Title
      </label>
    </div>
    <div class="md:w-5/6">
      <input class="form-text"
        id="title" name="title" type="text" value="{{ $title }}" {{ $disabled }}>
    </div>
  </div>

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="form-label md:text-right mb-1" for="url">
        Url
      </label>
    </div>
    <div class="md:w-5/6">
      <input class="form-text"
        id="url" name="url" type="text" value="{{ $url }}" {{ $disabled }}>
    </div>
  </div>

  <div class="flex items-center">
    <div class="md:w-1/6">
    </div>
    <div class="md:w-5/6 flex items-start">

      @if ($parent === 'create')
        <input class="btn" type="submit" name="Add" value="Add">
      @elseif ($parent === 'show')
        <a class="text-oxblood hover:text-black font-r mr-6" href="{{ $urlActionEdit }}">Edit</a>
        <a class="text-oxblood hover:text-black font-r mr-6" href="{{ $urlActionDelete }}">Delete</a>
        <a class="text-oxblood hover:text-black font-r" href="{{ $urlWebsite }}" target="_blank">Website</a>
      @elseif ($parent === 'edit')
        <input class="btn" type="submit" name="Update" value="Update">
      @elseif ($parent === 'delete')
        <input class="btn" type="submit" name="Delete" value="Delete">
      @endif

    </div>
  </div>

</form>
