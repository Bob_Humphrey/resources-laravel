@extends('layouts.app')

@section('content')

@php
  $urlActionUpdate = action('ItemController@update', ['id' => $item->id]);
  $title = (count($errors)) ? old('title') : $item->title;
  $url = (count($errors)) ? old('url') : $item->url;
  $categoryId = (count($errors)) ? old('category_id') : $item->category_id;
  foreach ($categories as $category) {
    $category->selected = ($category->id == $categoryId) ? 'selected' : '';
  }
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans mb-2">Edit Resource</h2>

    @include('item.form', [
      'parent' => 'edit',
      'formMethod' => 'POST',
      'formAction' => $urlActionUpdate
      ])

  </div>
</div>

@endsection
