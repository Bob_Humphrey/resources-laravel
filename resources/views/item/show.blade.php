@extends('layouts.app')

@section('content')

@php
  $urlActionDelete = action('ItemController@delete', ['id' => $item->id]);
  $urlActionEdit = action('ItemController@edit', ['id' => $item->id]);
  $urlActionShow = action('ItemController@show', ['id' => $item->id]);
  $urlWebsite = $item->url;
  $title = $item->title;
  $url = $item->url;
  $disabled = 'disabled';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans mb-2">View Resource</h2>

    @include('item.form', [
      'parent' => 'show',
      'formMethod' => 'GET',
      'formAction' => $urlActionShow
      ])

  </div>
</div>

@endsection
