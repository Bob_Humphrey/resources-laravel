@extends('layouts.app')

@section('content')

@php
  $urlStore = action('ItemController@store');
  $title = old('title');
  $url = old('url');
  // Determine if one of the categories was previously selected.
  $selectCategories = $categories->map(function ($category, $key) {
    $category->selected = (old('category_id') == $category->id) ? 'selected' : '';
    return $category;
  });
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3">
    <h2 class="font-sans mb-4 text-oxblood">Add Resource</h2>

    @include('item.form', [
      'parent' => 'create',
      'formMethod' => 'POST',
      'formAction' => $urlStore
      ])

  </div>
</div>

@endsection
