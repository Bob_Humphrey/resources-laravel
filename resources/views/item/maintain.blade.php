@php
  $categoryCount = $categories->count();
  $itemCount += ($categoryCount * 4);
  $displayedItemCount = 0;
  $currentColumn = 1;
@endphp


@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3">
    <h2 class="text-oxblood mb-6">Resources Maintenance</h2>
    <div class="lg:flex -mx-8">
      <div class="w-full lg:w-1/2 px-8">

        @foreach($categories as $category)

          @php
            $items = $category->items->sortBy('title');
            $displayedItemCount += ($items->count() + 4);
            $counter = 0;
          @endphp

          <h3 class="font-sans text-2xl text-oxblood font-bold mb-3">
            {{ $category->name }}
          </h3>
          <table class="w-full mb-12">
            @foreach($items as $item)
              @php
                $counter++;
                $bg_color = ($counter % 2) ? "bg-grain" : "bg-white";
                $truncatedTitle = str_limit($item->title, 25);
                $urlActionShow   = action('ItemController@show',   ['id' => $item->id]);
                $urlActionEdit   = action('ItemController@edit',   ['id' => $item->id]);
                $urlActionDelete = action('ItemController@delete', ['id' => $item->id]);
              @endphp
              <tr>
                <td class="w-2/3 table-cell {{ $bg_color }}">
                  <a class="font-r text-black hover:text-oxblood no-underline"
                    href="{{ $item->url }}" target="_blank">
                    {{ $truncatedTitle }}
                  </a>
                </td>
                <td class="w-1/3 table-cell text-center {{ $bg_color }}">
                  <a class="text-black no-underline px-2" href="{{ $urlActionShow }}"><i class="far fa-eye"></i></a>
                  <a class="text-black no-underline px-2" href="{{ $urlActionEdit }}"><i class="far fa-edit"></i></a>
                  <a class="text-black no-underline" href="{{ $urlActionDelete }}"><i class="far fa-trash-alt"></i></a>
                </td>
              </tr>
            @endforeach
          </table>

          @if (($currentColumn === 1) && ($displayedItemCount > ($itemCount / 2)))
            </div>
            <div class="w-full lg:w-1/2 px-8">
            @php
              $currentColumn = 2;
            @endphp
          @endif

        @endforeach

      </div>
    </div>
  </div>
</div>

@endsection
