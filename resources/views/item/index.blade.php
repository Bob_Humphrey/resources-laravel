@php
  $categoryCount = $categories->count();
  $categoriesPerColumn = ceil($categoryCount / 2);
  $categoriesDisplayed = 0;
  $currentColumn = 1;
@endphp


@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3">

    <nav class="flex justify-between text-oxblood mb-6">
      <div>
        <h2>Resources</h2>
      </div>
      <div class="flex items-center text-right">
        <div class="show-all text-2xl cursor-pointer"><i class="fas fa-chevron-circle-down"></i></div>
        <div class="hide-all text-2xl pl-6 cursor-pointer"><i class="fas fa-chevron-circle-up"></i></div>
      </div>
    </nav>

    <div class="lg:flex -mx-8">
      <div class="w-full lg:w-1/2 px-8">

        @foreach($categories as $category)

          @php
            $items = $category->items->sortBy('title');
            $categoriesDisplayed++;
            @endphp

          <div>

            <h3 class="category font-sans text-2xl text-oxblood font-bold border-b-4
            border-grain pb-2 mb-3 cursor-pointer">
              {{ $category->name }}
            </h3>

            <div class="items mb-12 hidden">
              @foreach($items as $item)
                <div class="mb-2">
                  <a class="font-r text-grey-darkest hover:text-oxblood no-underline"
                    href="{{ $item->url }}" target="_blank">
                    {{ $item->title }}
                  </a>
                </div>
              @endforeach
            </div>

          </div>

          @if (($currentColumn === 1) && ($categoriesDisplayed >= ($categoriesPerColumn)))
            </div>
            <div class="w-full lg:w-1/2 px-8">
            @php
              $currentColumn = 2;
            @endphp
          @endif

        @endforeach

      </div>
    </div>
  </div>
</div>

@endsection
