@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">

    <h2 class="font-sans mb-4">Login</h2>

    <form class="form" method="POST" action="{{ route('login') }}">

      {{ csrf_field() }}

      <div class="md:flex items-center mb-3">
        <div class="md:w-1/6">
          <label class="form-label md:text-right mb-1" for="email">
            Email
          </label>
        </div>
        <div class="md:w-5/6">
          <input class="form-text mb-1"
            id="email" type="email" name="email" value="{{ old('email') }}" >
        </div>
      </div>

      <div class="md:flex items-center mb-3">
        <div class="md:w-1/6">
          <label class="form-label md:text-right mb-1" for="password">
            Password
          </label>
        </div>
        <div class="md:w-5/6">
          <input class="form-text mb-1"
            id="password" type="password" name="password" >
        </div>
      </div>

      <div class="md:flex items-center">
        <div class="md:w-1/6">
        </div>
        <div class="md:w-5/6 flex items-start">
            <input class="btn" type="submit" name="Login" value="Login">
        </div>
      </div>

    </form>

  </div>
</div>
@endsection
