@extends('layouts.app')

@section('content')


<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans text-oxblood mb-4">About</h2>
    <div class="font-r text-grey-darkest leading-normal">
      <p>
        I use this application to keep track of my ever-growing
        list of web development resources.
      </p>



      <h3 class="font-sans text-black mb-4 mt-4">Credits</h3>
      <div class="font-r text-lavendar leading-normal">
        The application was built with:
        <ul>
          <li>PHP 7.2.7</li>
          <li>Laravel 5.6</li>
          <li>MySQL</li>
          <li>Tailwind CSS 0.6.5</li>
          <li>Umbrella JS</li>
          <li>Font Awesome Icons</li>
        </ul>
      </div>
    </div>
  </div>
</div>

@endsection
