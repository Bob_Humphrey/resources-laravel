@extends('layouts.app')

@section('content')

@php
  $urlActionUpdate = action('CategoryController@update', ['id' => $category->id]);
  $name = old('name') ? old('name') : $category->name;
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans mb-4">Edit Category</h2>

    @include('category.form', [
      'parent' => 'edit',
      'formMethod' => 'POST',
      'formAction' => $urlActionUpdate
      ])

  </div>
</div>

@endsection
