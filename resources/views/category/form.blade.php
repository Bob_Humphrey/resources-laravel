<form class="form" method="{{ $formMethod }}"
action="{{ $formAction }}">

  {{ csrf_field() }}

  @if ($parent === 'edit')
    <input name="_method" type="hidden" value="PATCH">
  @elseif ($parent === 'delete')
    <input name="_method" type="hidden" value="DELETE">
  @endif

  <div class="md:flex items-center mb-3">
    <div class="md:w-1/6">
      <label class="form-label md:text-right mb-1" for="name">
        Name
      </label>
    </div>
    <div class="md:w-5/6">
      <input class="form-text"
        id="name" name="name" type="text" value="{{ $name }}" {{ $disabled }}>
    </div>
  </div>


  <div class="flex items-center">
    <div class="md:w-1/6">
    </div>
    <div class="md:w-5/6 flex items-start">

      @if ($parent === 'create')
        <input class="btn" type="submit" name="Add" value="Add">
      @elseif ($parent === 'edit')
        <input class="btn" type="submit" name="Update" value="Update">
      @elseif ($parent === 'delete')
        <input class="btn" type="submit" name="Delete" value="Delete">
      @endif

    </div>
  </div>

</form>
