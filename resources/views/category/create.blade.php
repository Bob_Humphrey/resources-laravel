@extends('layouts.app')

@section('content')

@php
  $urlStore = action('CategoryController@store');
  $name = old('name');
  $disabled = '';
@endphp

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3 ">
    <h2 class="font-sans mb-4 text-oxblood">Add Category</h2>

    @include('category.form', [
      'parent' => 'create',
      'formMethod' => 'POST',
      'formAction' => $urlStore
      ])

  </div>
</div>

@endsection
