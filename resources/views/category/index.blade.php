@php
  $counter = 0;
  $column = 1;
  $categoriesCountBreak = $categories->count() / 2;
@endphp

@extends('layouts.app')

@section('content')

<div class="flex justify-center w-full">
  <div class="w-5/6 lg:w-2/3">
    <h2 class="text-oxblood mb-6">Categories</h2>
    <div class="lg:flex -mx-8">
      <div class="w-full lg:w-1/2 px-8">
        <table class="w-full">

          @foreach ($categories as $category)
            @php
              $counter++;
              $bg_color = ($counter % 2) ? "bg-grain" : "bg-white";
              $urlActionEdit   = action('CategoryController@edit',   ['id' => $category->id]);
              $urlActionDelete = action('CategoryController@delete', ['id' => $category->id]);
            @endphp
            <tr>
              <td class="table-cell w-3/4 {{ $bg_color }}">{{ $category->name }}</td>
              <td class="table-cell w-1/4 text-center {{ $bg_color }}">
                <a class="text-black no-underline px-2" href="{{ $urlActionEdit }}"><i class="far fa-edit"></i></a>
                <a class="text-black no-underline" href="{{ $urlActionDelete }}"><i class="far fa-trash-alt"></i></a>
              </td>
            </tr>
            @if (($column === 1) && ($counter >= $categoriesCountBreak))
                </table>
              </div>
              <div class="w-full lg:w-1/2 px-8">
                <table class="w-full">
              @php
                $column = 2;
              @endphp
            @endif
          @endforeach

        </table>
      </div>

    </div>
  </div>
</div>

@endsection
