@if ($flash = session('status'))

<div class="flex justify-center w-full mb-4">
  <div class="w-9/10 lg:w-2/3 font-r leading-normal bg-green-lighter border border border-green-lighter rounded p-6">
    {{ $flash }}
  </div>
</div>

@endif
