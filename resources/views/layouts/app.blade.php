@php
  $env = env('APP_ENV');
  $logoUrl = asset('img/bh-logo-oxblood.gif');
  $cssTw = asset('css/tw.css');
  $cssFontawesome = asset('css/all.min.css');
  $cssMin = asset('css/app.min.css');
  $jsCustom = asset('js/custom.js');
  $jsUmbrella = asset('js/umbrella.js');
  $jsMin = asset('js/app.min.js');
  $urlAbout = route('about');
  $urlHome = action('ItemController@index');
  $urlCreateItem = action('ItemController@create');
  $urlItemMaintenance = action('ItemController@maintain');
  $urlCategories = action('CategoryController@index');
  $urlCreateCategory = action('CategoryController@create');
  $urlLogin = route('login');
  $urlLogout = action('LogoutController@logout');
  $urlLoginLogout = (Auth::check()) ? $urlLogout : $urlLogin;
@endphp

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="description" content="An application for maintaining
      a useful collection of web development resources.">
      <title>
        Web Development Resources
      </title>

      @if ($env === 'local')
        <!-- Fontawesome -->
        <link rel="stylesheet" href="{{ $cssFontawesome }}">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ $cssTw }}">
      @else
        <link rel="stylesheet" href="{{ $cssMin }}">
      @endif
    </head>


  <body class="bg-white">

    {{-- SMALL AND MEDIUM SCREENS --}}

    @php
      $iconColor = 'text-grey-darkest';
    @endphp

    <nav class="lg:hidden bg-grain mb-12 py-6 text-oxblood">
      <div class="flex justify-center ">
        <div class="text-center mb-6">
          <a class="text-oxblood no-underline" href="{{ $urlHome }}">
            <h1 class="font-sans">Web Development Resources</h1>
          </a>
        </div>
      </div>
      <div class="flex justify-center text-xl">
        @include('layouts.iconmenu')
      </div>
    </nav>

    {{-- LARGE SCREENS --}}

    <nav class="hidden lg:flex justify-between lg:bg-grain mb-12 px-12
     py-6 text-oxblood">
      <div>
        <a class="text-black hover:text-oxblood no-underline" href="{{ $urlHome }}">
          <h1 class="font-sans leading-none">Web Development Resources</h1>
        </a>
      </div>
      <div class="flex items-center text-right">
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlAbout }}">About</a></div>
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlHome }}">Resources</a></div>
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlCreateItem }}" title="Add Resource"><i class="fas fa-plus-circle"></i></a></div>
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlItemMaintenance }}" title="Maintain Resources"><i class="fas fa-database"></i></a></div>
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlCategories }}">Categories</a></div>
        <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
          href="{{ $urlCreateCategory }}"><i class="fas fa-plus-circle"></i></a></div>
        @if (Auth::check())
          <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
            href="{{ $urlLogout }}">Logout</a></div>
        @else
          <div class="font-r px-2"><a class="text-oxblood hover:text-black no-underline"
            href="{{ $urlLogin }}">Login</a></div>
        @endif

      </div>
    </nav>

    <main>

      @include('layouts.flash')

      @include('layouts.errors')

      @yield('content')

    </main>

    <footer class="py-16 bg-tan mt-12">
      <div class="flex justify-center w-full">
        <a href="https://bob-humphrey.com/">
          <img class="w-16 m-auto" src="{{ $logoUrl }}" alt="Bob Humphrey"/>
        </a>
      </div>
    </footer>

    @if ($env === 'local')
      <script src="{{ $jsUmbrella }}"></script>
      <script src="{{ $jsCustom }}"></script>
    @else
    <script src="{{ $jsMin }}"></script>
    @endif
  </body>
</html>
