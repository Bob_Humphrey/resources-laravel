<div>
  <a class="text-black no-underline px-2" href="{{ $urlAbout }}" title="About">
    <i class="fas fa-info-circle"></i>
  </a>
  <a class="text-black no-underline px-2" href="{{ $urlHome }}" title="Resources">
    <i class="fas fa-link"></i>
  </a>
  <a class="text-black no-underline px-2" href="{{ $urlCreateItem }}" title="Add Resource">
    <i class="fas fa-plus-circle"></i>
  </a>
  <a class="text-black no-underline px-2" href="{{ $urlItemMaintenance }}" title="Maintain Resources">
    <i class="fas fa-database"></i>
  </a>
  <a class="text-black no-underline px-2" href="{{ $urlCategories }}" title="Categories">
    <i class="fas fa-tags"></i>
  </a>
  <a class="text-black no-underline px-2" href="{{ $urlCreateCategory }}" title="Add Category">
    <i class="fas fa-plus-circle"></i>
  </a>
  <a class="{{ $iconColor }} no-underline px-2" href="{{ $urlLoginLogout }}" title="Login/Logout">
    <i class="fas fa-unlock-alt"></i>
  </a>
</div>
