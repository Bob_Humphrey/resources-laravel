<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Item;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('items')->delete();
      $json = File::get('database/data/resources.json');
      $data = json_decode($json);
      foreach ($data as $obj) {
        $categoryRecord = Category::where('name', $obj->category)->get()->first();
        $categoryId = $categoryRecord->id;
        Item::create(array(
          'title' => $obj->name,
          'url' => $obj->link,
          'category_id' => $categoryId
        ));
      }
    }
}
