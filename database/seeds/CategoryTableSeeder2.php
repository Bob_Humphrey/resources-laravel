<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->delete();
      $json = File::get('database/data/resources.json');
      $data = json_decode($json);
      foreach ($data as $obj) {
        $records = Category::where('name', $obj->category)->get();
        $count = $records->count();
        if ($count === 0) {
          Category::create(array(
            'name' => $obj->category
          ));
        }
      }
    }
}
